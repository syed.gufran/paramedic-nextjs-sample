import React, { useState } from "react";

function SignIn() {
  const [data, setData] = useState("");

  const [email, setEmail] = useState("");

  const [password, setPassword] = useState("");

  const SignInStaging = () => {
    var raw = JSON.stringify({
      email: email,
      password: password,
    });

    var requestOptions = {
      method: "POST",
      body: raw,
    };

    fetch("/api/signinnew", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        console.log({ result });

        setData(result.login.response);

        if (result.login.status === "Success") {
          localStorage.setItem("loginData", JSON.stringify(result));
        } else if (result.login.status === "Failure") {
          console.log("Good work keep going");
        }
      })
      .catch((error) => console.log("error", error));
  };

  const SignInApiOld = () => {
    var raw = JSON.stringify({
      email: email,
      password: password,
    });

    var requestOptions = {
      method: "POST",
      body: raw,
    };

    fetch("/api/signinoldurl", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        console.log({ result });

        setData(result.login.response);

        if (result.login.status === "Success") {
          localStorage.setItem("loginData", JSON.stringify(result));
        } else if (result.login.status === "Failure") {
          console.log("Good work keep going");
        }
      })
      .catch((error) => console.log("error", error));
  };

  return (
    <div>
      <input
        id="emailNumber"
        type="text"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        placeholder="Mobile Number / Email"
      />

      <input
        id="password"
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        placeholder="Password"
      />

      <button onClick={SignInApiOld}>
        https://staging-cloud.lifecell.in/graphql
      </button>

      <button onClick={SignInStaging}>
        http://104.211.245.76/staging/graphql{" "}
      </button>
    </div>
  );
}

export default SignIn;
