// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default (req, res) => {
  console.log("req.body", req.body);

  const body = JSON.parse(req.body);

  const { email, password } = body;

  var myHeaders = new Headers();

  myHeaders.append("Authorization", "Bearer 7vvvkdzygqw5rbzjkwc3kult14vv2ula");

  myHeaders.append("Content-Type", "application/json");

  myHeaders.append(
    "Cookie",
    "PHPSESSID=54b920ff8f9e2d8a735134d883aff6a6; private_content_version=c5abe2336cdf6670bfbc8f06e860e0c7"
  );

  var graphql = JSON.stringify({
    query: `{login( phoneno:"", email :"${email}",password:"${password}"){status,response}}`,
    variables: {},
  });

  var requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: graphql,
    redirect: "follow",
  };

  fetch("https://staging-cloud.lifecell.in/graphql", requestOptions)
    .then((response) => response.json())
    .then((result) => {
      console.log(result);
      res.status(200).json(result.data);
    })
    .catch((error) => console.log("error", error));
};
